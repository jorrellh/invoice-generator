class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :description
      t.decimal :hours
      t.decimal :rate
      t.references :invoice, foreign_key: true
      t.timestamps
    end
  end
end
