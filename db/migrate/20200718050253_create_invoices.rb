class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.decimal :total
      t.string :notes
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
