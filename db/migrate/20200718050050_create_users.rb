class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :company_name
      t.string :address
      t.integer :abn
      t.string :string
      t.string :email
      t.timestamps
    end
  end
end
