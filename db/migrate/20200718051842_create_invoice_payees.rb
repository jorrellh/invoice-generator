class CreateInvoicePayees < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_payees do |t|
      t.integer :invoice_id
      t.integer :payee_id
      t.timestamps
    end
  end
end
