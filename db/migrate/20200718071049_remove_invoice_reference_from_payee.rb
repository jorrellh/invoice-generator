class RemoveInvoiceReferenceFromPayee < ActiveRecord::Migration[6.0]
  def change
    remove_reference :payees, :invoice
  end
end
