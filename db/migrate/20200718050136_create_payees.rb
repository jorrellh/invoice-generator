class CreatePayees < ActiveRecord::Migration[6.0]
  def change
    create_table :payees do |t|
      t.string :name
      t.string :company
      t.string :phone
      t.string :email
      t.string :address
      t.references :invoice, foreign_key: true
      t.timestamps
    end
  end
end
