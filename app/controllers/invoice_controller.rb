class InvoiceController < ApplicationController
    def index
        # Only support one user per deployment.
        @invoices = User.first.invoices.order("created_at DESC")
        @user = User.first

    end

    def new
        @invoice = Invoice.new
    end

    def show
        @invoice = Invoice.find(params[:id])
        respond_to do |format|
            format.html
            format.pdf do
                render pdf: "Invoice #{@invoice.id}"   # Excluding ".pdf" extension.
            end
        end
    end

    def create
        @payee = Payee.find(params[:invoice][:payee][:payee_id])
        # Holy fuck, what a hack
        params[:invoice].delete(:payee)
        @invoice = Invoice.create(invoice_params.merge(user_id: User.first.id))
        @ip = InvoicePayee.create(invoice_id: @invoice.id, payee_id: @payee.id)
        
        if @invoice.save
            @ip.save
            redirect_to invoice_path(@invoice), notice: "Invoice created"
        else
            redirect_to new_invoice_path, notice: "An error occurred."
        end
        
    end

    private

    def invoice_params
        
        params.require(:invoice).permit(:total, :notes)
    end
end
