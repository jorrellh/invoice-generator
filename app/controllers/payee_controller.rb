class PayeeController < ApplicationController
    def index
        @payees = Payee.all
        @payee = Payee.new
    end

    def show
        @payee = Payee.find(params[:id])
    end

    def new
        @payee = Payee.new
    end

    def update
        @payee = Payee.find(params[:id])
        if @payee.update(payee_params)
            redirect_to payee_path(@payee), notice: 'Payee updated'
        else
            redirect_to payee_path(@payee), notice: 'Error occurred'

        end

    end

    def create
        @payee = Payee.create(payee_params)
        if @payee.save
            redirect_to payee_path(@payee), notice: 'Payee updated'
        else
            redirect_to payee_path(@payee), notice: 'Error occurred'

        end

    end

    private

    def payee_params
        params.require(:payee).permit(:name, :company, :phone, :email, :address)
    end
end
