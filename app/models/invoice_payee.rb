class InvoicePayee < ApplicationRecord
    belongs_to :payee
    belongs_to :invoice
end
