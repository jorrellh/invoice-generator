class Payee < ApplicationRecord
    has_many :invoice_payees
    has_many :invoices, :through => :invoice_payees
end
