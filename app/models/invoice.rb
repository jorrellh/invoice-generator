class Invoice < ApplicationRecord
    before_create :randomize_id
    belongs_to :user
    has_many :items
    has_one :invoice_payee
    has_one :payee, :through => :invoice_payee

    private
    def randomize_id
        begin
            self.id = SecureRandom.random_number(1_000_000)
        end while Invoice.where(id: self.id).exists?
    end
end
