Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :invoice
  resources :user
  resources :payee
  root to: 'invoice#index'
end
